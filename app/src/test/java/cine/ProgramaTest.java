package cine;

import org.junit.Test;
import static org.junit.Assert.*;

import java.time.LocalDate;

public class ProgramaTest {
    @Test
    public void testeoPrograma() {
        LocalDate fecha_ini = LocalDate.of(2023,04,15);
        LocalDate fecha_fin = LocalDate.of(2023, 05, 15);
        Programacion programa1 = new Programacion(fecha_ini, fecha_fin, 17, "La monja");
        assertEquals(programa1.toString(), "La monja");
    }
}
