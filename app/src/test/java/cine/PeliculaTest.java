package cine;

import org.junit.Test;
import static org.junit.Assert.*;

public class PeliculaTest {
    @Test
    public void testeoPeli() {
        Pelicula pelicula = new Pelicula("Terror", "La monja", 2, 18, "Ricardo");
        assertEquals(pelicula.toString(), "Terror");
    }
}
