package cine;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;

public class CineTest {
    @Test
    public void testeoCine() {
        ArrayList<String> program1 = new ArrayList<String>();
        program1.add("La monja");
        Cine cine = new Cine(program1, "Cinemacenter", 80, 10);
        assertEquals(cine.toString(), "Cinemacenter");
    }
}
