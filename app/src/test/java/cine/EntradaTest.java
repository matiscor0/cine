package cine;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class EntradaTest {
    @Test
    public void testeoEntrada() {
        LocalDate fech_venta = LocalDate.of(2023,4, 20);
        LocalDate fech_funcion = LocalDate.of(2023,6, 28);
        Entrada entrada = new Entrada(1234, fech_venta, 10, "La monja", fech_funcion, 17, 1500, 18, "5 estrellas");
        
        assertEquals(entrada.toString(), "La monja");
    }

}
