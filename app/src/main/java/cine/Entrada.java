package cine;

import java.time.LocalDate;

public class Entrada {
    private Integer nro_ticket;
    private LocalDate fecha_venta;
    private Integer nro_funcion;
    private String nombre_pelicula;
    private LocalDate fecha_funcion;
    private Integer hora_funcion;
    private Integer costo;
    private Integer edad_min;
    private String calificacion;

    public Entrada(int nro_ticket, LocalDate fecha_venta, Integer nro_funcion, String nombre_pelicula,
            LocalDate fecha_funcion, Integer hora_funcion, Integer costo, Integer edad_min, String calificacion) {
        this.nro_ticket = nro_ticket;
        this.fecha_venta = fecha_venta;
        this.nro_funcion = nro_funcion;
        this.nombre_pelicula = nombre_pelicula;
        this.fecha_funcion = fecha_funcion;
        this.hora_funcion = hora_funcion;
        this.costo = costo;
        this.edad_min = edad_min;
        this.calificacion = calificacion;
    }

    public Integer getNro_ticket() {
        return nro_ticket;
    }

    public void setNro_ticket(Integer nro_ticket) {
        this.nro_ticket = nro_ticket;
    }

    public Integer getNro_funcion() {
        return nro_funcion;
    }

    public void setNro_funcion(Integer nro_funcion) {
        this.nro_funcion = nro_funcion;
    }

    public String getNombre_pelicula() {
        return nombre_pelicula;
    }

    public void setNombre_pelicula(String nombre_pelicula) {
        this.nombre_pelicula = nombre_pelicula;
    }

    public LocalDate getFecha_funcion() {
        return fecha_funcion;
    }

    public void setFecha_funcion(LocalDate fecha_funcion) {
        this.fecha_funcion = fecha_funcion;
    }

    public Integer getHora_funcion() {
        return hora_funcion;
    }

    public void setHora_funcion(Integer hora_funcion) {
        this.hora_funcion = hora_funcion;
    }

    public Integer getCosto() {
        return costo;
    }

    public void setCosto(Integer costo) {
        this.costo = costo;
    }

    public Integer getEdad_min() {
        return edad_min;
    }

    public void setEdad_min(Integer edad_min) {
        this.edad_min = edad_min;
    }

    public LocalDate getFecha_venta() {
        return fecha_venta;
    }

    public void setFecha_venta(LocalDate fecha_venta) {
        this.fecha_venta = fecha_venta;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String toString() {
        return nombre_pelicula;
    }

}