package cine;

public class Butaca {
    private String nro_asiento;
    private Boolean estado;

    public Butaca(String nro_asiento, Boolean estado) {
        this.nro_asiento = nro_asiento;
        this.estado = estado;
    }

    public String getNro_asiento() {
        return nro_asiento;
    }

    public void setNro_asiento(String nro_asiento) {
        this.nro_asiento = nro_asiento;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String toString() {
        return nro_asiento;
    }
}
