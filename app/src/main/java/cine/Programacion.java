package cine;

import java.time.LocalDate;

public class Programacion {
    private LocalDate fecha_ini;
    private LocalDate fecha_fin;
    private Integer hora_funcion;
    private String nombre_pelicula;

    public Programacion(LocalDate fecha_ini, LocalDate fecha_fin, Integer hora_funcion, String nombre_pelicula) {
        this.fecha_ini = fecha_ini;
        this.fecha_fin = fecha_fin;
        this.hora_funcion = hora_funcion;
        this.nombre_pelicula = nombre_pelicula;
    }

    public LocalDate getFecha_ini() {
        return fecha_ini;
    }
    public void setFecha_ini(LocalDate fecha_ini) {
        this.fecha_ini = fecha_ini;
    }
    public LocalDate getFecha_fin() {
        return fecha_fin;
    }
    public void setFecha_fin(LocalDate fecha_fin) {
        this.fecha_fin = fecha_fin;
    }
    public Integer getHora_funcion() {
        return hora_funcion;
    }
    public void setHora_funcion(Integer hora_funcion) {
        this.hora_funcion = hora_funcion;
    }
    public String getNombre_pelicula() {
        return nombre_pelicula;
    }

    public void setNombre_pelicula(String nombre_pelicula) {
        this.nombre_pelicula = nombre_pelicula;
    }
    
    public String toString() {
        return nombre_pelicula;
    }
}
