package cine;

public class Espectador {
    final String nombre;
    private Integer edad;
    private float dinero;
    
    public Espectador(final String nombre, Integer edad, float dinero) {
        this.nombre = nombre;
        this.edad = edad;
        this.dinero = dinero;
    }

    public String getNombre() {
        return nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public float getDinero() {
        return dinero;
    }

    public void setDinero(float dinero) {
        this.dinero = dinero;
    }

    public String toString() {
        return nombre;
    }

}
