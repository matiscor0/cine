package cine;

public class Salon {
    final Integer limite_butaca;
    private Integer cant_uso;
    
    public Salon(Integer limite_butaca, Integer cant_uso) {
        this.limite_butaca = limite_butaca;
        this.cant_uso = cant_uso;
    }

    public Integer getCant_uso() {
        return cant_uso;
    }
    
    public void setCant_uso(Integer cant_uso) {
        this.cant_uso = cant_uso;
    }
}
