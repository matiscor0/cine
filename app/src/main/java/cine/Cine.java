package cine;

import java.util.ArrayList;

public class Cine {
    private ArrayList<String> funcion_disponible = new ArrayList<String>();
    private String nombre_cine;
    final Integer limite_butaca;
    private Integer cant_uso;

    public Cine(ArrayList<String> funcion_disponible, String nombre_cine, Integer limite_butaca, Integer cant_uso) {
        this.funcion_disponible = funcion_disponible;
        this.nombre_cine = nombre_cine;
        this.limite_butaca = limite_butaca;
        this.cant_uso = cant_uso;
    }


    public ArrayList<String> getFuncion_disponible() {
        return funcion_disponible;
    }


    public void setFuncion_disponible(ArrayList<String> funcion_disponible) {
        this.funcion_disponible = funcion_disponible;
    }

    public void setAgregarFuncion(String funcion) {
        this.funcion_disponible.add(funcion);
    }

    public String getNombre_cine() {
        return nombre_cine;
    }


    public void setNombre_cine(String nombre_cine) {
        this.nombre_cine = nombre_cine;
    }

    public Integer getCant_uso() {
        return cant_uso;
    }


    public void setCant_uso(Integer cant_uso) {
        this.cant_uso = cant_uso;
    }

    public Integer getLimite_butaca() {
        return limite_butaca;
    }

    public String toString() {
        return nombre_cine;
    }

}
