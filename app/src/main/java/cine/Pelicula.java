package cine;

public class Pelicula {
    private String genero;
    private String titulo;
    private Integer duracion;
    private Integer edad_min_actores_prin;
    private String director;
       
    public Pelicula(String genero, String titulo, Integer duracion, Integer edad_min_actores_prin, String director) {
        this.genero = genero;
        this.titulo = titulo;
        this.duracion = duracion;
        this.edad_min_actores_prin = edad_min_actores_prin;
        this.director = director;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public Integer getEdad_min_actores_prin() {
        return edad_min_actores_prin;
    }

    public void setEdad_min_actores_prin(Integer edad_min_actores_prin) {
        this.edad_min_actores_prin = edad_min_actores_prin;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String toString() {
        return genero;
    }
}

